package main

import (
	"github.com/bwmarrin/discordgo"
	"log"
)

type discrodClient struct {
	session *discordgo.Session
	config  Config
}

const (
	CMDAddChannel    = "add-tg-channel"
	CmdaddchannelUrl = "ch-url"
)

var (
	commands = []*discordgo.ApplicationCommand{
		{
			Name:        CMDAddChannel,
			Description: "Add telegram channel",
			NameLocalizations: &map[discordgo.Locale]string{
				discordgo.Ukrainian: "додати-тг-канал",
			},
			DescriptionLocalizations: &map[discordgo.Locale]string{
				discordgo.Ukrainian: "Додати телеграм канал",
			},
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        CmdaddchannelUrl,
					Description: "Url of the channel, which should be added",
					Required:    true,
					NameLocalizations: map[discordgo.Locale]string{
						discordgo.Ukrainian: "лінка-каналу",
					},
					DescriptionLocalizations: map[discordgo.Locale]string{
						discordgo.Ukrainian: "Лінка на канал",
					},
				},
			},
		},
	}
)

func getCommandOptionsAsMap(options []*discordgo.ApplicationCommandInteractionDataOption) map[string]*discordgo.ApplicationCommandInteractionDataOption {
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}
	return optionMap

}
func (c *discrodClient) setupDiscrodCommand() {
	if err := c.session.Open(); err != nil {
		log.Fatalln("Can not open session ", err)
	}
	for _, v := range commands {
		if _, err := c.session.ApplicationCommandCreate(c.session.State.User.ID, "", v); err != nil {
			log.Println(err)
		}
	}
	defer func(goBot *discordgo.Session) {
		_ = goBot.Close()
	}(c.session)

	log.Println("Created discord bot")
}

func initDiscordClient(config Config) discrodClient {
	client := discrodClient{config: config}
	bot, err := discordgo.New("Bot " + config.DiscordToken)
	if err != nil {
		log.Fatalln("Can not init bot ", err)
	}
	client.session = bot

	client.setupDiscrodCommand()

	return client
}
