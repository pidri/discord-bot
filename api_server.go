package main

import (
	"bytes"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"sync"
)

type ApiServer struct {
	config  Config
	engine  *gin.Engine
	discord *discrodClient
	db      *dbClient
}

func (s *ApiServer) handleInteractionPing(c *gin.Context) {
	log.Println("get ping request")
	c.JSON(http.StatusOK, discordgo.InteractionResponse{Type: discordgo.InteractionResponsePong})
}

func interactionResponse(c *gin.Context, textMessage string) {
	c.JSON(http.StatusOK, discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{Content: textMessage},
	})
}

func (s *ApiServer) handleCommand(c *gin.Context, body *discordgo.InteractionCreate) {
	cmd := body.ApplicationCommandData()
	options := getCommandOptionsAsMap(cmd.Options)
	switch cmd.Name {
	case CMDAddChannel:
		if tgUrlOption, ok := options[CmdaddchannelUrl]; ok {
			tgUrl := tgUrlOption.Value.(string)
			r := regexp.MustCompile(`(?m)https://t\.me/(?P<username>[^/]*)`)
			rMatch := r.FindStringSubmatch(tgUrl)
			if len(rMatch) != 2 {
				interactionResponse(c, fmt.Sprintf("Invalid url (%s) to group message.", tgUrl))
				return
			}
			username := rMatch[1]

			chanelInfo, tgChanelID := s.db.getChannelByUsername(username)
			if chanelInfo == nil {
				post, err := http.Post(
					fmt.Sprintf("%s/add_channel/", s.config.TgClientWebApi),
					"application/json",
					bytes.NewReader([]byte(fmt.Sprintf(`{ "username": "%s" }`, username))),
				)
				if err != nil || post.StatusCode >= 300 {
					interactionResponse(c, fmt.Sprintf("Can not find channel %s", username))
					return
				}

				chanelInfo, tgChanelID = s.db.getChannelByUsername(username)
				if chanelInfo == nil {
					interactionResponse(c, fmt.Sprintf("Something bad happens, not joined to channel %s", username))
				}
			}
			err := s.db.addChatSubscriber(tgChanelID, body.ChannelID)
			if err != nil {
				interactionResponse(c, fmt.Sprintf("Something bad happens, not joined to channel %s", username))
			}
			interactionResponse(c, fmt.Sprintf("Subscribed to %s", username))
			return
		} else {
			interactionResponse(c, "Can not process given data")
			return
		}
	}
	//	https://t.me/Kyiv_by_Grishyn/5931
	c.JSON(http.StatusOK, discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{Content: "ok"},
	})
}

func (s *ApiServer) broadcastMessage(c *gin.Context, ) {
	var requestBody = struct {
		MessageID string `json:"msg_id"`
	}{}

	if err := c.BindJSON(&requestBody); err != nil {
		c.Status(http.StatusBadRequest)
		log.Println("Can not parse body for broadcastMessage")
		return
	}
	msg, err := s.db.getMessage(requestBody.MessageID)
	if err != nil {
		log.Printf("Can not find message by ID `%s`\n", requestBody.MessageID)
		c.Status(http.StatusBadRequest)
		return
	}
	tgChannel, err := s.db.getTgChannelInfo(strconv.FormatInt(msg.ChannelID, 10))
	if err != nil {
		log.Printf("Can not find telegram channel by id `%d`\n", msg.ChannelID)
		c.Status(http.StatusBadRequest)
		return
	}

	bindings, err := s.db.getBindingForChannel(msg.ChannelID)
	if err != nil {
		log.Printf("Can npt find binding for channel with id `%d`\n", msg.ChannelID)
		c.Status(http.StatusBadRequest)
		return
	}

	discordMsg := fmt.Sprintf("`%s:`\n%s ", tgChannel.Title, msg.Text)

	var wg sync.WaitGroup
	for _, b := range bindings {
		wg.Add(len(b.Members))
		for _, m := range b.Members {
			recepientID := m.DiscordChatID
			go func() {
				_, err := s.discord.session.ChannelMessageSend(recepientID, discordMsg)
				if err != nil {
					fmt.Printf("Can not send message. Receipient: %s, tgChannel %d", recepientID, msg.ChannelID)
				}
				wg.Done()
			}()
		}
	}
	wg.Wait()
	c.Status(http.StatusAccepted)
}
func (s *ApiServer) apiPing(c *gin.Context, ) {
	c.String(http.StatusAccepted, "pong")
}

func (s *ApiServer) apiCallHandler(c *gin.Context, ) {
	if !discordgo.VerifyInteraction(c.Request, s.config.hexPubKey()) {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	var body discordgo.InteractionCreate

	if err := c.BindJSON(&body); err != nil {
		log.Println("Can not parse input body", err)
		return
	}
	switch body.Type {
	case discordgo.InteractionPing:
		s.handleInteractionPing(c)
	case discordgo.InteractionApplicationCommand:
		s.handleCommand(c, &body)
	default:
		c.String(http.StatusOK, "Ok")

	}
}

func (s *ApiServer) RunServer() {
	_ = s.engine.Run(fmt.Sprintf(":%d", s.config.ApiPort))
}

func (s *ApiServer) requestHandler(w http.ResponseWriter, r *http.Request) {
	s.engine.ServeHTTP(w, r)
}

func setupApiServer(config Config, discord *discrodClient, db *dbClient) ApiServer {
	server := ApiServer{
		config:  config,
		engine:  gin.Default(),
		discord: discord,
		db:      db,
	}
	server.engine.POST("/api/bot/", server.apiCallHandler)
	server.engine.POST("/api/handle_new_msg/", server.broadcastMessage)

	server.engine.POST("/api/ping/", server.apiPing)
	server.engine.GET("/api/ping/", server.apiPing)
	server.engine.POST("/", server.apiPing)
	server.engine.GET("/", server.apiPing)

	return server
}
//https://pidri-discord-bot-4qa2fibrga-ey.a.run.app/api/bot/
//https://79a2-176-36-207-91.ngrok-free.app/api/bot/
//https://b410-176-36-207-91.ngrok-free.app/api/bot/
