package main

import (
	"cloud.google.com/go/firestore"
	"context"
	"log"
	"strconv"
)

var (
//	ChannelInfoNotFoundError = errors.New("CanNotSaveChannel")
)

const (
	TGChannelInfoCollectionName = "Channels"
	BindingCollectionName       = "Bindings"
	TgMessagesCollectionName    = "Messages"
)

type dbClient struct {
	fsClient   *firestore.Client
	context    context.Context
	channels   *firestore.CollectionRef
	bindings   *firestore.CollectionRef
	tgMessages *firestore.CollectionRef
}

type ChannelBindingMemberModel struct {
	DiscordChatID  string `firestore:"discord_chid"`
}

type BindingModel struct {
	IsTopLevel    bool                        `firestore:"istop"`
	MembersNumber int                         `firestore:"members_number"`
	ChatID        int64                       `firestore:"tg_chat_id"`
	Members       []ChannelBindingMemberModel `firestore:"members"`
}

type TGMessage struct {
	Date      int    `firestore:"data"`
	Text      string `firestore:"text"`
	GroupedID int64  `firestore:"group_id"`
	ChannelID int64  `firestore:"channel_id"`
	MessageID int    `firestore:"msg_id"`
}

type TGChannelInfoModel struct {
	Username   string `firestore:"username"`
	JoinedUser string `firestore:"joined_user"`
	Title      string `firestore:"title"`
}

func (db *dbClient) getChannelByUsername(username string) (*TGChannelInfoModel, int64) {
	channelDocs, err := db.channels.Where("username", "==", username).Limit(1).Documents(db.context).GetAll()
	if err != nil || len(channelDocs) < 1 {
		return nil, 0
	}
	var result TGChannelInfoModel
	if err := channelDocs[0].DataTo(&result); err != nil {
		return nil, 0
	}

	channelID, err := strconv.ParseInt(channelDocs[0].Ref.ID, 10, 64)
	if err != nil {
		return nil, 0
	}

	return &result, channelID
}

func (db *dbClient) getMessage(msgID string) (msg TGMessage, err error) {
	doc, err := db.tgMessages.Doc(msgID).Get(db.context)
	if err != nil {
		return TGMessage{}, err
	}

	if err := doc.DataTo(&msg); err != nil {
		return TGMessage{}, err
	}
	return
}

func (db *dbClient) getTgChannelInfo(tgChannelID string) (tgChannel TGChannelInfoModel, err error) {
	doc, err := db.channels.Doc(tgChannelID).Get(db.context)
	if err != nil {
		return TGChannelInfoModel{}, err
	}

	if err := doc.DataTo(&tgChannel); err != nil {
		return TGChannelInfoModel{}, err
	}
	return
}

func (db *dbClient) getBindingForChannel(tgChannelID int64) (binding []BindingModel, err error) {
	docs, err := db.bindings.Where("tg_chat_id", "==", tgChannelID).Documents(db.context).GetAll()
	if err != nil {
		return nil, err
	}
	result := make([]BindingModel, len(docs))
	for i, d := range docs {
		if err := d.DataTo(&result[i]); err != nil {
			return nil, err
		}
	}
	return result, nil
}

func (db *dbClient) addChatSubscriber(tgChannelID int64, chatID string) error {
	//TODO(Yura Braiko): For now just one or zero binding per tg channel. In feature will be some sort of balancing.
	newMember := ChannelBindingMemberModel{ DiscordChatID:  chatID }
	log.Print("Start adding new member")
	bindingQuery := db.bindings.Where("tg_chat_id", "==", tgChannelID)
	err := db.fsClient.RunTransaction(db.context, func(ctx context.Context, tx *firestore.Transaction) error {
		docs, err := tx.Documents(bindingQuery).GetAll()
		if err != nil {
			log.Printf("error, during seelcting bindings %e", err)
			return err
		}
		log.Printf("Selecting existing members, (selected %d records)", len(docs))
		if len(docs) == 0 {
			err := tx.Create(db.bindings.NewDoc(), BindingModel{
				IsTopLevel:    true,
				MembersNumber: 1,
				ChatID:        tgChannelID,
				Members:       []ChannelBindingMemberModel{newMember},
			})
			if err != nil {
				log.Printf("error, during saving new bindings %e", err)
				return err
			}
			log.Print("New doc created")
		} else {
			isExists := false
			for _, d:= range docs{
				if isExists{
					break
				}
				var record BindingModel
				err := d.DataTo(&record)
				if err != nil {
					continue
				}
				for _, m := range record.Members {
					if m.DiscordChatID == chatID {
						isExists = true
						break
					}
				}
			}
			if isExists {
				log.Println("Seems this channel already binded ")
			}else{
				err := tx.Update(docs[0].Ref, []firestore.Update{
					{Path: "members", Value: firestore.ArrayUnion(newMember)},
					{Path: "members_number", Value: firestore.Increment(1)},
				})
				if err != nil {
					log.Println("Error with adding new user ")
					return err
				}
			}
		}

		return nil
	})
	return err
}

func initDbClient(config Config) *dbClient {
	ctx := context.Background()
	fsClient, err := firestore.NewClient(ctx, config.GrpcProjectID)
	if err != nil {
		log.Panic("Can not init firebase client ", err)
	}
	return &dbClient{
		fsClient:   fsClient,
		context:    ctx,
		channels:   fsClient.Collection(TGChannelInfoCollectionName),
		bindings:   fsClient.Collection(BindingCollectionName),
		tgMessages: fsClient.Collection(TgMessagesCollectionName),
	}
}
