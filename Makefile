WORKDIR := $(shell pwd)
.ONESHELL:
.EXPORT_ALL_VARIABLES:
DOCKER_BUILDKIT=1


__CHECK:=$(shell \
	mkdir -p $(WORKDIR)/.build; \
)

help: ## Display help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'
	
run: build ## run client 
	docker run --rm \
		-p 8073:8073 \
		-v $(WORKDIR)/config.yaml:/app/config.yaml \
		-v ~/.config:/root/.config \
		pidri-discord-bot 
	
build: .build/img ## Build docker image 

.build/img: Dockerfile go.mod go.sum
	docker build -t pidri-discord-bot .
	touch .build/img
	
push-stage: ## Push image to stage registry 
# 	docker tag tg-client europe-west3-docker.pkg.dev/pidri-stage/tg-client
	docker tag docker.io/library/pidri-discord-bot  europe-west3-docker.pkg.dev/pidri-stage/pidri-stage-dkr/pidri-discord-bot
	docker push europe-west3-docker.pkg.dev/pidri-stage/pidri-stage-dkr/pidri-discord-bot

deploy-stage: ## Deploy staging 
	gcloud run deploy pidri-discord-bot	 \
		--image europe-west3-docker.pkg.dev/pidri-stage/pidri-stage-dkr/pidri-discord-bot:latest \
		--cpu 1 --memory 300Mi \
		--min-instances 0 \
		--service-account pidri-discord-bot@pidri-stage.iam.gserviceaccount.com \
		--env-vars-file env_configs/stage.yaml \
		--region europe-west3 \
		--allow-unauthenticated \
		--project=pidri-stage


# e2-micro	