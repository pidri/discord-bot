package main

import (
	"log"
)

func main() {
	conf, confError := GetConfig()
	if confError != nil {
		log.Fatalln("Can not read config", confError)
	}

	discord := initDiscordClient(conf)
	db := initDbClient(conf)
	api := setupApiServer(conf, &discord, db)

	api.RunServer()
}

/*
yurabraiko
gcloud compute ssh tg-clinet 	--project=pidri-stage --zone=europe-west3-c
*/