package main

import (
	"encoding/hex"
	"fmt"
	"github.com/spf13/viper"
	"os"
)

type Config struct {
	DiscordPublicKey string `mapstructure:"DISCORD_PUBLIC_KEY"`
	DiscordToken     string `mapstructure:"DISCORD_TOKEN"`
	GrpcProjectID    string `mapstructure:"GRPC_PROJECT_ID"`
	ApiPort          int    `mapstructure:"API_PORT"`
	TgClientWebApi   string `mapstructure:"TG_CLIENT_WEB_API"`
}

func (c Config) hexPubKey() []byte {
	r, e := hex.DecodeString(c.DiscordPublicKey)
	if e != nil {
		fmt.Printf("Error during geting pub key %e", e)
	}
	return r
}

func GetConfig() (config Config, err error) {
	if _, e := os.Stat("./config.yaml"); e == nil && !os.IsNotExist(e) {
		v := viper.New()
		v.SetConfigFile("config.yaml")
		v.AddConfigPath("./")

		if e := v.ReadInConfig(); e != nil {
			fmt.Printf("Isse with reading config: %e", e)
			return Config{}, e
		}
		if e := v.Unmarshal(&config); e != nil {
			fmt.Printf("Isse with parsing tp the config: %e", e)
			return Config{}, e
		}
	}
	v := viper.New()
	v.AutomaticEnv()
	_ = v.BindEnv("DISCORD_TOKEN")
	_ = v.BindEnv("DISCORD_PUBLIC_KEY")
	_ = v.BindEnv("GRPC_PROJECT_ID")
	_ = v.BindEnv("API_PORT")
	_ = v.BindEnv("TG_CLIENT_WEB_API")

	if e := v.Unmarshal(&config); e != nil {
		fmt.Printf("Isse with parsing tp the config: %e", e)
		return Config{}, e
	}

	return config, nil
}
